import { useEffect, useState } from "react";
import { db } from './firebase'
import { 
  collection,getDocs, 
  addDoc,updateDoc,doc,
  deleteDoc 
        } from 'firebase/firestore'

function App() {
  // Storing Data
  const [users, setusers] = useState([])

  // Push Data to DB
  const [newName, setnewName] = useState('')
  const [newAge, setnewAge] = useState(0)

  // Users Ref.
  const usersREF = collection(db, 'users')

  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usersREF);
      console.log(data.docs)
      setusers(data.docs.map(e=> ({...e.data(),id:e.id,})))
    }

    getUsers()
  },[])

  // Create User Button
  const createUser = async () => {
    await addDoc(usersREF,{name:newName,age:newAge})
  }

  // Update Users Data 
  const updateData = async (id,age) => {
    const userDoc = doc(db,'users',id)
    const newFields = {age: age+1}
    await updateDoc(userDoc,newFields)

  }

  // Delete a user
  const deleteBtn = async (id) => {
    const userDoc = doc(db,'users',id)
    await deleteDoc(userDoc)
  }

  return (
    <> 
    <input onChange={(e)=> setnewName(e.target.value)} 
    type='text' placeholder="Type Name..." />

    <input onChange={(e)=> setnewAge(e.target.value)}  
    type='number' placeholder="Type Age..." />

    <button onClick={createUser}> Create User </button>

    {users.map((e)=> {
      return( 
      <div> 
        <h1> Name : {e.name} |  Age : {e.age} </h1>
        <button onClick={()=> updateData(e.id,parseInt(e.age))}> INC Age </button>
        <button onClick={()=> deleteBtn(e.id)}> Delete </button>
        
      </div>
    )})}
    </>
  );
}

export default App;
